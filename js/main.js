$(function() {
    $('.btn')
        .on('mouseenter', function(e) {
            var parentOffset = $(this).offset(),
                relX = e.pageX - parentOffset.left,
                relY = e.pageY - parentOffset.top;
            $(this).find('span').css({top:relY, left:relX})
        })
        .on('mouseout', function(e) {
            var parentOffset = $(this).offset(),
                relX = e.pageX - parentOffset.left,
                relY = e.pageY - parentOffset.top;
            $(this).find('span').css({top:relY, left:relX})
        });
    $('[href=#]').click(function(){return false});
});


$(".btn-modal").fancybox({
    'padding'    : 0
});


$('.btn-scroll').click(function(){
    var str=$(this).attr('href');
    $.scrollTo(str, 500);
    return false;
});


$('.partners').slick({
    dots: false,
    arrows: true,
    loop: true,
    slidesToShow: 6,
    slidesToScroll: 1,
    prevArrow: '<span class="slider-nav prev"></span>',
    nextArrow: '<span class="slider-nav next"></span>',
    responsive: [
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 4,
                arrows: true
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 2,
                arrows: true
            }
        }
    ]
});

$('.review').slick({
    dots: false,
    arrows: true,
    loop: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    prevArrow: '<span class="slider-nav prev"></span>',
    nextArrow: '<span class="slider-nav next"></span>',
    responsive: [
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 2,
                arrows: true
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                arrows: true
            }
        }
    ]
});

ymaps.ready(init);


function init () {
    var myMap = new ymaps.Map("map", {
        center: [57.990691613518095,56.24460626585387],
        zoom: 16,
        controls: ['smallMapDefaultSet']
    });

    myMap.geoObjects
        .add(new ymaps.Placemark([57.99080556660266,56.247149000000014], {
            balloonContent: 'г. Пермь, ул. Куйбышева 95Б'
        }, {
            preset: 'islands#icon',
            iconColor: '#0095b6'
        }));
}


$('.btn-send').click(function() {

    $('body').find('form:not(this)').children('div').removeClass('red'); //удаление всех сообщение об ошибке(валидатора)
    var answer = checkForm($(this).closest('form').get(0)); //ответ от валидатора
    if(answer != false)
    {
        var $form = $(this).closest('form'),
            type      =     $('input[name="type"]', $form).val(),
            name      =     $('input[name="name"]', $form).val(),
            phone     =     $('input[name="phone"]', $form).val();

        $.ajax({
            type: "POST",
            url: "form-handler.php",
            data: {name: name, phone: phone, type:type}
        }).done(function(msg) {
            $('form').find('input[type=text], textarea').val('');
            console.log('удачно');
            $('.btn-thanks').trigger('click');
        });
    }
});



var Android = navigator.userAgent.search(/Android/i);
var iPhone = navigator.userAgent.search(/iPhone/i);
var iPad = navigator.userAgent.search(/iPad/i);
if(Android != -1 || iPhone != -1 || iPad != -1) {
    console.log('mobile');
} else {

    wow = new WOW(
        {
            animateClass: 'animated',
            offset:       100,
            callback:     function(box) {
                console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
            }
        }
    );
    wow.init();


    $(".scroll").each(function () { // анимация по скролу(используйте этот) достаточно добавить анимируемому блоку класс 'scroll' а css анимацию писать так: '.animated.класс_блока'
        var block = $(this);
        $(window).scroll(function() {
            var top = block.offset().top;
            var bottom = block.height()+top;
            top = top - $(window).height();
            var scroll_top = $(this).scrollTop();
            if ((scroll_top > top) && (scroll_top < bottom)) {
                if (!block.hasClass("scroll-animated")) {
                    block.addClass("scroll-animated");
                }
            } else {
                block.removeClass("scroll-animated");
            }
        });
    });
}